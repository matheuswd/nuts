<?php

// Load the NUTS Framework engine. This line has to stay at the top of functions.php
require_once "framework/nuts.php";

// If you want to hide the Framework Options page, define the constant below:
// define( 'HIDE_FRAMEWORK_OPTIONS', true );




// Register sidebars
add_action( 'widgets_init', 'nuts_widgets_init' );

function nuts_widgets_init() {

	$args = array(
		'name'          => __( 'Right sidebar', 'nuts-starter' ),
		'id'            => 'right-sidebar',
		'description'   => __( 'Sidebar displayed in the right column', 'nuts-starter' ),
		'class'         => 'right-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);
	register_sidebar( $args );

	$args = array(
		'name'          => __( 'Footer sidebar #1', 'nuts-starter' ),
		'id'            => 'footer-sidebar-1',
		'class'         => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);
	register_sidebar( $args );

	$args = array(
		'name'          => __( 'Footer sidebar #2', 'nuts-starter' ),
		'id'            => 'footer-sidebar-2',
		'class'         => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);
	register_sidebar( $args );

	$args = array(
		'name'          => __( 'Footer sidebar #3', 'nuts-starter' ),
		'id'            => 'footer-sidebar-3',
		'class'         => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);
	register_sidebar( $args );

	$args = array(
		'name'          => __( 'Footer sidebar #4', 'nuts-starter' ),
		'id'            => 'footer-sidebar-4',
		'class'         => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);
	register_sidebar( $args );

}



// Replaces the excerpt "more" text with a link
function nuts_excerpt_more( $more ) {

	global $post;

	return ' <a class="readmore" href="'. get_permalink( $post->ID ) . '">'. esc_html( nuts_get_value ( 'readmore' ) ) .'</a>';

}
add_filter( 'excerpt_more', 'nuts_excerpt_more' );



// Run the SETUP process
function nuts_setup () {

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	// This is a HTML5 theme. Make some WP functions know it.
	current_theme_supports( 'html5' );
	// Handle title tags in the WP 4.1 way
	add_theme_support( 'title-tag' );
	// Add some configuration to the theme
	add_theme_support( 'post-thumbnails' );
	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	set_post_thumbnail_size( 780, 350, true, array( 'left', 'center' ) );
	// Add extra image sizes
	add_image_size( 'homepage-thumb', 220, 180, false );

	// Load the Theme textdomain for localization
	load_theme_textdomain( 'nuts-starter', get_template_directory() . '/languages' );

	// Register the menu locations
	register_nav_menu( 'primary', __( 'Primary Menu', 'nuts-starter' ) );

	// Set up the default content width
	if ( !isset( $content_width ) ) $content_width = 780;

}
add_action ( 'after_setup_theme', 'nuts_setup' );


add_filter( 'document_title_separator', 'nuts_document_title_separator' );
function nuts_document_title_separator(){
    return '|';
}


// Custom Comment output
function nuts_comment( $comment, $args, $depth ) {
	$GLOBALS["comment"] = $comment;
	extract( $args, EXTR_SKIP );

	$tag = 'article';
	$add_below = 'comment';

	?>
	<<?php echo $tag ?> <?php comment_class( empty( $args["has_children"] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID(); ?>" itemscope itemtype="http://schema.org/Comment">
		<div class="comment-inner">
			<figure class="gravatar"><?php echo get_avatar( $comment, $args["avatar_size"] ); ?></figure>
			<div class="comment-meta" role="complementary">
				<p class="comment-author"><?php _e( 'Posted on', 'nuts-starter' ); ?> <time class="comment-meta-item" datetime="<?php comment_date( 'Y-m-d' ); ?>T<?php comment_time( 'H:iP' ); ?>" itemprop="datePublished"><?php comment_date( 'jS F Y' ); ?>, <?php comment_time(); ?></time> <?php
					if ( get_comment_author_url() != '' ) {
						$cauthor_text = '<a class="comment-author-link" href="'. get_comment_author_url() .'" itemprop="author">' . get_comment_author() . '</a>';
					}
					else {
						$cauthor_text = get_comment_author();
					}
					printf( __( 'by %s', 'nuts-starter' ), $cauthor_text );
					?></p>
				<div class="comment-reply">
					<?php $comment_id = null;
					if ( $depth == $args["max_depth"] ) {
						$comment_id = $comment->comment_parent;
						$depth--;
					}
					comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args["max_depth"] ) ), $comment_id ); ?>
					<?php edit_comment_link( '<small>' . __( 'Edit this comment', 'nuts-starter' ) . '</small>' ); ?>
				</div>
				<?php if ( $comment->comment_approved == '0' ) { ?>
				<p class="comment-meta-item"><?php _e( 'Your comment is awaiting moderation.', 'nuts-starter' ); ?></p>
				<?php } ?>
			</div>
			<div class="comment-content" itemprop="text">
				<?php comment_text(); ?>
			</div>
		</div>
	<?php
}

function nuts_comment_end() {
	echo '</article>';
}

function nuts_hamburger_icon( $for_id, $class = '' ) {
	echo '<div class="hamburger-icon';
	if ( $class != '' ) {
		echo ' ' . $class;
	}
	echo '" data-for="'. $for_id .'">
	<span class="hamburger-line"></span>
	<span class="hamburger-line"></span>
	<span class="hamburger-line"></span>
</div>';
}
