=== NUTS Starter ===
Contributors: Wholegrain Digital
Requires at least: WordPress 4.3
Tested up to: WordPress 4.4.2
Version: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: black, two-columns, right-sidebar, responsive-layout, custom-colors, custom-header, custom-menu, featured-images, threaded-comments, translation-ready

== Description ==
NUTS Starter is a starter theme for the NUTS Framework. NUTS Framework is a code package for WordPress theme developers with a goal to make their lives easier.

Key features:
* Theme Options system
* Theme Options can be used in templates, post content, CSS and JS files
* Built-in LESS compiler - connected with your Theme Options
* Responsive Layout
* Custom Colors
* Pre-defined styles for the most common HTML and WP elements
* Can be made extremely customizable while keeping it cacheable
* GPL v2.0 license. Use it to make something cool.

For more information about NUTS please go to http://www.wholegraindigital.com/nuts/

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Click on Upload Theme and upload your downloaded copy of the NUTS Starter ZIP file.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to http://www.wholegraindigital.com/nuts/ for a guide on how to customize this theme.
5. Navigate to Appearance > Theme Options in your admin panel where you can customize it.

== Copyright ==

NUTS Starter WordPress Theme, Copyright 2014-2016 Scamper Ltd
NUTS Starter is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

NUTS Starter theme bundles the following third-party resources:

Less.php v1.7.0.9, Copyright 2015 Josh Schmidt
Licenses: Apache License v2.0
Source: https://github.com/oyejorge/less.php


== Changelog ==

http://www.wholegraindigital.com/nuts/changelog/
