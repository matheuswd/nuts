<?php get_header(); ?>

<div id="contentWrapper" class="row">
    <div id="content" class="content column-8">

		<?php get_template_part ( 'loop', 'blog' ); ?>

    </div><!-- content -->

    <?php get_sidebar(); ?>

</div><!-- contentWrapper -->

<?php get_footer();
