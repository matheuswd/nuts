<div class="search-box">
	<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url() ; ?>/">
		<input class="searchinput" type="text" value="" placeholder="<?php _e( 'Search', 'nuts-starter' ) ?>" name="s" id="s" />
		<input class="searchsubmit" type="submit" id="searchsubmit" value="<?php _e( 'Go', 'nuts-starter' ) ?>" />
	</form>
</div>
