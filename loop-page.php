<?php
/* Loop for pages displaying static pages */

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() ) { ?><div class="featimage"><?php the_post_thumbnail(); ?></div><?php } ?>
		<h1<?php if ( !has_post_thumbnail() ) { echo ' class="nofeat"'; } ?>><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<?php wp_link_pages(); ?>
	</article>

<?php endwhile; else: ?>

	<p><?php _e( 'Sorry, no posts matched your criteria.', 'nuts-starter' ); ?></p>

<?php endif;
