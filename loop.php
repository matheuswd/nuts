<?php
/* Loop for pages displaying single post */

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() ) { ?><div class="featimage"><?php the_post_thumbnail(); ?></div><?php } ?>
		<h1<?php if ( !has_post_thumbnail() ) { echo ' class="nofeat"'; } ?>><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<?php wp_link_pages(); ?>
		<?php if ( nuts_get_value ( 'source' ) ) echo '<p>'. esc_html( nuts_get_value ( 'source_label' ) ) .' <a href="'. esc_url( nuts_get_value ( 'source' ) ) .'">'. esc_html( nuts_get_value ( 'source' ) ) .'</a></p>' ?>
		<aside class="postmeta"><?php
			$author = get_the_author();
			$date = get_the_date( 'M j, Y' );
			printf ( __('Posted by %1$s on %2$s' ,'nuts-starter'), $author, $date ); ?>
		</aside>
		<aside class="tags">
			<?php the_tags(); ?>
		</aside>
	</article>

<?php endwhile; else: ?>

	<p><?php _e( 'Sorry, no posts matched your criteria.', 'nuts-starter' ); ?></p>

<?php endif;
