<?php

// The file must have the type-[data-type].php filename format


define	( "NUTS_DEFAULT_TEXT_SIZE",	"");
define	( "NUTS_TEXT_LESS_STRING",	true );




// The function must have the nuts_type_[data-type]_field name format
function nuts_type_text_field ( $name, $value ) {

	global $nuts_options_array;

	if ( $nuts_options_array[$name]["size"] != NULL ) $size = $nuts_options_array[$name]["size"];
		else $size = NUTS_DEFAULT_TEXT_SIZE;

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( isset( $nuts_options_array[$name]["placeholder"] ) ) $placeholder = $nuts_options_array[$name]["placeholder"];
		else $placeholder = '';

	echo '<div class="text">
			'. $prefix .'<input type="text" name="' . nuts_form_ref ( $name ) . '" id="' . $name . '" value="' . esc_attr( $value ) . '"';

	if ( $size > 0 ) echo ' maxlength="' . intval( $size ) . '"';
	if ( $placeholder != '' ) echo ' placeholder="' . esc_attr( $placeholder ) . '"';

	echo ' />'. $suffix .'
		</div>';

}



// Returns the text value
function nuts_get_text ( $name ) {

	global $nuts_options_array;

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( nuts_get_option( $name ) == '' ) {
		$prefix = '';
		$suffix = '';
	}

	return $prefix . nuts_get_option( $name ) . $suffix;

}




// Prints the text
function nuts_text ( $name ) {

	echo esc_html( nuts_get_text ( $name ) );

}
