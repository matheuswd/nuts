<?php

// The file must have the type-[data-type].php filename format


define	( "NUTS_STATIC_LESS_STRING",	true );




// The function must have the nuts_type_[data-type]_field name format
function nuts_type_static_field ( $name, $value ) {

	global $nuts_options_array;

	if ( $value == '' ) $value = $nuts_options_array[$name]["default"];

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	echo '<div class="static">'. $prefix . $value . $suffix .'</div>';

}



// Returns the static value
function nuts_get_static ( $name ) {

	global $nuts_options_array;

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( nuts_get_option( $name ) == '' ) {
		$prefix = '';
		$suffix = '';
	}

	return $prefix . nuts_get_option( $name ) . $suffix;

}




// Prints the static value
function nuts_static ( $name ) {

	echo esc_html( nuts_get_text ( $name ) );

}
