<?php

// The file must have the type-[data-type].php filename format





// The function must have the nuts_type_[data-type]_field name format
function nuts_type_select_field ( $name, $id ) {

	global $nuts_options_array;

	if ( ( $id == '' ) && ( isset( $nuts_options_array[$name]["default"] ) ) ) {
		$id = $nuts_options_array[$name]["default"];
	}

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( isset( $nuts_options_array[$name]["placeholder"] ) ) $placeholder = $nuts_options_array[$name]["placeholder"];
		else $placeholder = '';

	echo '<div class="select">
	' . $prefix . '<select name="' . nuts_form_ref ( $name ) . '" id="' . $name . '" autocomplete="off">';

	if ( $placeholder != '' ) echo '<option value="">' . $placeholder . '</option>';

	foreach ( $nuts_options_array[$name]["values"] as $key => $value ) {
		echo '<option value="' . esc_attr( $key ) . '"';

		if ( $key == $id ) echo ' selected';

		echo '>' . esc_html( $value ) . '</option>';

	}

	echo '</select>' . $suffix . '
	</div>';

}



// Gets the value of the saved option
function nuts_get_select ( $name, $key = null ) {

	global $nuts_options_array;

	$id = nuts_get_option ( $name );

	if ( isset( $nuts_options_array[$name]["prefix"] ) ) $prefix = $nuts_options_array[$name]["prefix"];
		else $prefix = '';
	if ( isset( $nuts_options_array[$name]["suffix"] ) ) $suffix = $nuts_options_array[$name]["suffix"];
		else $suffix = '';

	if ( $key == null && gettype( $key ) != 'boolean' ) {

		if ( isset( $nuts_options_array[$name]["key"] ) ) {
			$key = $nuts_options_array[$name]["key"];
		}
		else {
			$key = false;
		}

	}

	if ( $key ) {
		return $id;
	}
	else {
		return $prefix . $nuts_options_array[$name]["values"][$id] . $suffix;
	}

}



// This is the function that prints the value
function nuts_select ( $name, $key = null ) {

	echo esc_html( nuts_get_select ( $name, $key ) );

}
