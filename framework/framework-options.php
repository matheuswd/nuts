<?php

// General tab in Framework Options
$section = array (
	"name"			=> 'framework_options::general',
	"title"			=> __( 'General', 'nuts' ),
	"description"	=> __( 'General Framework Options', 'nuts' ),
	"tab"			=> __( 'General', 'nuts' ),
);
nuts_register_section ( $section );


// Framework version
$nuts_option_array = array (
        "name"			=> 'framework_version',
        "title"			=> __( 'Framework version', 'nuts' ),
        "section"		=> 'framework_options::general',
        "type"			=> 'static',
        "default"		=> NUTS_VERSION,
		"prefix"		=> 'v',
);
nuts_register_option ( $nuts_option_array );


// Theme version
$nuts_current_theme = wp_get_theme();
$nuts_option_array = array (
        "name"			=> 'theme_version',
        "title"			=> __( 'Theme version', 'nuts' ),
        "section"		=> 'framework_options::general',
        "type"			=> 'static',
        "default"		=> $nuts_current_theme->version,
		"prefix"		=> 'v',
);
nuts_register_option ( $nuts_option_array );


// Turn LESS compiler on/off
$nuts_option_array = array (
        "name"			=> 'less_compiler',
        "title"			=> __( 'Less Compiler', 'nuts' ),
        "description"	=> __( '<b>ON:</b> Less files are compiled upon each page reload, even if there was no change (slowest, suitable for development environments)<br>
							<b>OFF:</b> Less files are NOT compiled in any circumstances (fastest, use it if you\'re not going to change styles or Theme Options for a long time, suitable for live sites with cached assets)<br>
							<b>Dynamic:</b> Less files are updated when there is a change in the Theme Options or inside the Less files (middle speed, use it in test environments or when you\'re changing options frequently on live sites.)', 'nuts' ),
        "section"		=> 'framework_options::general',
        "type"			=> 'select',
		"values"		=> array(
								"on" 		=> __( 'ON', 'nuts' ),
								"off" 		=> __( 'OFF', 'nuts' ),
								"dynamic" 	=> __( 'Dynamic', 'nuts' ),
							),
        "default"		=> 'on',
        "key"           => true,
		"less"			=> false,
);
nuts_register_option ( $nuts_option_array );
