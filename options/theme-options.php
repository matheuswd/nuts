<?php


// The "nuts_logo" is a special (unique) option. It can be reached with the nuts_logo() function in the frontend - if defined
$nuts_logo = array (
        "name"			=> 'nuts_logo',
        "title"			=> __( 'Theme Logo', 'nuts-starter' ),
        "description"	=> __( 'Please upload your website\'s logo here.', 'nuts-starter' ),
        "section"		=> 'header',
        "type"			=> 'image',
        "size"			=> 'homepage-thumb',
);
nuts_register_option ( $nuts_logo );


// The margin above the logo
$nuts_option_array = array (
        "name"			=> 'starter_topmargin',
        "title"			=> __( 'Top margin above the logo', 'nuts-starter' ),
        "description"	=> __( 'Enter the top margin value in pixels.', 'nuts-starter' ),
        "section"		=> 'header',
        "type"			=> 'number',
        "step"			=> '1',
        "min"			=> '0',
        "max"			=> '200',
        "default"		=> '105',
        "prefix"		=> '',
        "suffix"		=> 'px',
        "placeholder"   => __( 'Enter a number here', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );



// Position of the logo: left, center or right
$nuts_option_array = array (
        "name"			=> 'logo_position',
        "title"			=> __( 'Horizontal position of the logo', 'nuts-starter' ),
        "description"	=> __( 'The logo will be placed to this side of the header', 'nuts-starter' ),
        "section"		=> 'header',
        "type"			=> 'select',
        "values"		=> array(
                                "left" => __( 'Left', 'nuts-starter' ),
                                "center" => __( 'Center', 'nuts-starter' ),
                                "right" => __( 'Right', 'nuts-starter' ),
                            ),
        "default"		=> 'left',
        "key"           => true,
);
nuts_register_option ( $nuts_option_array );



// Color field
$nuts_option_array = array (
        "name"			=> 'acolor',
        "title"			=> __( 'Theme basic color scheme', 'nuts-starter' ),
        "description"	=> __( 'Please select a color. It will be the base of your color scheme. Change it and test how your site responds to it.', 'nuts-starter' ),
        "section"		=> 'colors',
        "type"			=> 'color',
        "default"		=> '#cccccc',
);
nuts_register_option ( $nuts_option_array );


// Color field
$nuts_option_array = array (
        "name"			=> 'h1color',
        "title"			=> __( 'Heading elements', 'nuts-starter' ),
        "description"	=> __( 'Please select a color for the heading (h1 .. h6) elements.', 'nuts-starter' ),
        "section"		=> 'colors',
        "type"			=> 'color',
        "default"		=> '#009fe3',
);
nuts_register_option ( $nuts_option_array );


// Color field
$nuts_option_array = array (
        "name"			=> 'bodyColor',
        "title"			=> __( 'Main body text', 'nuts-starter' ),
        "description"	=> __( 'You can modify the body text color here. Use this as @bodyColor variable in your LESS files', 'nuts-starter' ),
        "section"		=> 'colors',
        "type"			=> 'color',
        "default"		=> '#666666',
);
nuts_register_option ( $nuts_option_array );


// Color field
$nuts_option_array = array (
        "name"			=> 'linkColor',
        "title"			=> __( 'Color for anchor tags / links inside the content area', 'nuts-starter' ),
        "description"	=> __( 'This color is linked by default to post content, but not to header and footer.', 'nuts-starter' ),
        "section"		=> 'colors',
        "type"			=> 'color',
        "default"		=> '#cccccc',
);
nuts_register_option ( $nuts_option_array );


// Should the links use inverse colors while hovering over them?
$nuts_option_array = array (
        "name"			=> 'link_hover',
        "title"			=> __( 'Link inverse effect', 'nuts-starter' ),
        "description"	=> __( 'Should the links use inverse colors while hovering the mouse over them?', 'nuts-starter' ),
        "section"		=> 'colors',
        "type"			=> 'select',
        "values"		=> array(
                                "no" => __( 'No', 'nuts-starter' ),
                                "yes" => __( 'Yes', 'nuts-starter' ),
                            ),
        "default"		=> 'no',
        "key"           => true,
);
nuts_register_option ( $nuts_option_array );





// The margin above the logo
$nuts_option_array = array (
        "name"			=> 'gridMaxWidth',
        "title"			=> __( 'Maximum width of the theme', 'nuts-starter' ),
        "description"	=> __( 'This is the maximum width in pixels, under this value the theme will behave responsively.', 'nuts-starter' ),
        "section"		=> 'other',
        "type"			=> 'number',
        "step"			=> '1',
        "min"			=> '320',
        "default"		=> '1200',
        "suffix"		=> 'px',
        "placeholder"   => __( 'Enter a number here', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );


// Text field
$nuts_option_array = array (
        "name"			=> 'source_label',
        "title"			=> __( 'Label for the source', 'nuts-starter' ),
        "description"	=> __( 'The text label that\'s displayed before the source link right after the post contents.', 'nuts-starter' ),
        "section"		=> 'other',
        "type"			=> 'text',
        "size"			=> '',
        "default"		=> __( 'Source:', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );


// Text field
$nuts_option_array = array (
        "name"			=> 'readmore',
        "title"			=> __( 'Read more text', 'nuts-starter' ),
        "description"	=> __( 'The read more text after the post excerpts in blog view.', 'nuts-starter' ),
        "section"		=> 'other',
        "type"			=> 'text',
        "size"			=> '',
        "default"		=> __( 'Read more...', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );


// Text field: File upload button label
$nuts_option_array = array (
        "name"			=> 'file_upload',
        "title"			=> __( 'File upload button text', 'nuts-starter' ),
        "description"	=> __( 'This text will be displayed on the file upload buttons.', 'nuts-starter' ),
        "section"		=> 'other',
        "type"			=> 'text',
        "size"			=> '',
        "default"		=> __( 'Upload File', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );



// Text field
$nuts_option_array = array (
        "name"			=> 'newerposts',
        "title"			=> __( 'Newer Posts link text', 'nuts-starter' ),
        "description"	=> __( 'Newer posts text used for pagination in blog view.', 'nuts-starter' ),
        "section"		=> 'other',
        "type"			=> 'text',
        "size"			=> '',
        "default"		=> __( 'Newer Posts', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );


// Text field
$nuts_option_array = array (
        "name"			=> 'olderposts',
        "title"			=> __( 'Older Posts link text', 'nuts-starter' ),
        "description"	=> __( 'Older posts text used for pagination in blog view.', 'nuts-starter' ),
        "section"		=> 'other',
        "type"			=> 'text',
        "size"			=> '',
        "default"		=> __( 'Older Posts', 'nuts-starter' ),
);
nuts_register_option ( $nuts_option_array );
